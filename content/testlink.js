
function uniqBy(a, key) {
    var seen = {};
    return a.filter(function(item) {
        var k = key(item);
        return seen.hasOwnProperty(k) ? false : (seen[k] = true);
    });
}

function extractJira(search_target){
	if(search_target.length){
		var jiras = search_target.html().match(/SPL-[0-9][0-9][0-9][0-9][0-9]/g);
		if(jiras == null){
			return []
		}
		jiras = uniqBy(jiras, function(a){ return a;});
		return jiras
	}
}

function load_note(root_url, exec_id){
	
	var url2load=root_url+'lib/execute/getExecNotes.php?readonly=1&exec_id=' + exec_id;
	load_note()
	return url2load;
}

function get_execute_history_id_list(root_url, testcase_id, call_back){
	var url = root_url + '/lib/execute/execHistory.php?tcase_id=' + testcase_id;
	$.get(url, function(data){

	});
}

function handle_navigator(pathname){
	if(pathname != "/testlink/lib/execute/execNavigator.php") 
		return;

	chrome.storage.sync.get(['username', 'test_note'], function(items){
		var myUserName = items['username'];
		console.log(myUserName);
		if(!(myUserName)){
			myUserName = '[Any]';
		}

		var priority = $("select[name='filter_priority']");

		var user = $("select[name='filter_assigned_user']");

		var form = $("#filter_panel_form");

		var myNameOptionString = "select[name='filter_assigned_user'] > option:contains('"+ myUserName +"')";
		console.log(myNameOptionString);
		var myNameOption = $(myNameOptionString);

		var myNameValue = "1";
		
		$("#filters > div").append('<button type="button" style="font-size: 90%;">My Cases</button>');
		$("#filters > div > button").click(function(){
			if(myNameOption.length){
				myNameValue = myNameOption.val();
			}

			if(priority.length){
				priority.val("3");
			}

			if(user.length){
				user.val(myNameValue);
			}

			if(form.length){
				$("#filter_panel_form").submit()
			}
		});
	});
}

function handle_detail_page(pathname){
	if(pathname != "/testlink/lib/execute/execSetResults.php")
		return
	
	//layout
	// $("#execution_history").hide();
	var titles_to_hide = ['Test plan notes', 'Platform description', 'Build description'];
	$.each(titles_to_hide, function(key, value){
		var header = $("span:contains(" + value + ")")
		if(header.length){
			header.parent().hide();
		}	
	});
	$("#toggle_history_on_off").parent().hide();
	$("h1").hide();
	$("hr").hide();
	$("div[id^='cfields_design_time_tcversionid_']").hide();
	$("div[id^='cfields_exec_time_tcversionid_']").hide();
	


	var testNote = $("table[class='invisible']").find("textarea[name^='notes']");

	if(testNote.length){
		chrome.storage.sync.get(['username', 'test_note'], function(items){
			testNote.val(items['test_note']);
		});
	}

	// history
	jiras = []

	search_target = $("#execution_history").find("*")
	jiras.concat(extractJira(search_target))

	$.each(jiras, function(key, value){
		$("body").prepend("<a target='_blank' href='http://jira.splunk.com/browse/"+ value +"' style='font-size: 180%;'>&nbsp;&nbsp;" + value +"</a>")	
	});
	
}


function update_test_note(notes){
	var pathname = window.location.pathname;
	if(pathname == "/testlink/lib/execute/execSetResults.php"){
		var testNote = $("table[class='invisible']").find("textarea[name^='notes']")

		if(testNote.length){
			testNote.val(notes);
		}
	}
}

$(function () {
	var pathname = window.location.pathname;
	var root_url = window.location.href;

	handle_navigator(pathname)
	handle_detail_page(pathname)

	// listen to popup string
	chrome.runtime.onMessage.addListener(
    	function(request, sender, sendResponse) {
        	if (request.test_note){
        		update_test_note(request.test_note);
        		handle_navigator(pathname);
        		sendResponse({farewell: "goodbye"});
            }
	});
});