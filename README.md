# README #

## This extension automatically log you in Splunk ##

## How to install ##
* Clone the repository
* On Chrome, go to settings-> extensions
* Check developer mode and upload the directory you cloned

## How to use ##
* Click on the extension's icon and set your username/password
* Click save
* Go to your instance!! Enjoy no more typing admin/changeme!

##  Important ##
* This extension works only when the url matches "*://localhost/*" and "*://*.sv.splunk.com/*".
* If you want it to support more domain, update it in manifest.json and then reload the extension