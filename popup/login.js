$(function(){
	chrome.storage.sync.get(['username', 'password'], function(items){
		$("#username").val(items['username'])
		$("#password").val(items['password'])
	});

	$("#save_button").click(function() {
		var usr = $("#username").val()
		var password = $("#password").val()
		var data_obj = {'username': usr, 'password': password}
		chrome.storage.sync.set(data_obj, function() {
        	chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
  				chrome.tabs.sendMessage(tabs[0].id, {password: password}, function(response) {
    				console.log(response.farewell);
  				});
			});
        });
	});
});

